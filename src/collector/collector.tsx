import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Blocks } from "./helpers/blocks";
import { ICollectorProps } from "./helpers/interfaces/props";
import { ICollectorSettings } from "./helpers/interfaces/settings";
import { buttons } from "./helpers/buttons";
import { progressbar } from "./helpers/progressbar";
import { pages } from "./helpers/pages";
import "./blocks";
import "./collector.scss";

export class Collector extends React.PureComponent<ICollectorProps> {
    /** Contains the blocks renderer instance. */
    readonly blocks = new Blocks(this.props.definition, this.props.mode || "paginated", this.props.snapshot, this.props.preview);

    /** Contains some settings for the collector (they can be changed during runtime). */
    readonly settings: ICollectorSettings = {
        enumerators: Tripetto.castToBoolean(this.props.enumerators),
        pages: Tripetto.castToBoolean(this.props.pages),
        buttons: this.props.buttons || "inline",
        progressbar: Tripetto.castToBoolean(this.props.progressbar)
    };

    /** Render the collector. */
    render(): React.ReactNode {
        return (
            <>
                <section className={`container${this.settings.buttons === "sticky" && this.blocks.status !== "preview" ? " sticky" : ""}`}>
                    <div className="row">
                        <div className="col">
                            {this.blocks.render(this.settings) ||
                                ((this.blocks.status === "empty" || this.blocks.status === "preview") && (
                                    <div className="text-center mt-5">
                                        <div className="text-center mt-5">
                                            <h3>👋 Nothing to show here yet</h3>
                                            <p className="text-secondary">Add blocks to the form first to get the magic going.</p>
                                        </div>
                                    </div>
                                )) ||
                                (this.blocks.status === "finished" && (
                                    <div className="text-center mt-5">
                                        <h3>✔ You’ve completed the form</h3>
                                        <p className="text-secondary">
                                            For the purpose of this demo the form output is visible in your browser’s developer console. Go
                                            there to see the collected data.
                                        </p>
                                    </div>
                                )) ||
                                (this.blocks.status === "stopped" && (
                                    <div className="text-center mt-5">
                                        <h3>⏹ You’ve stopped the form</h3>
                                        <p className="text-secondary">Press the play icon to start a new session.</p>
                                    </div>
                                )) ||
                                (this.blocks.status === "paused" && (
                                    <div className="text-center mt-5">
                                        <h3>⏸ You’ve paused the form</h3>
                                        <p className="text-secondary">
                                            For the purpose of this demo the paused form is saved in your browser’s local store. Refresh the
                                            browser to resume the paused form.
                                        </p>
                                    </div>
                                )) || (
                                    <div className="text-center mt-5">
                                        <h3>⏹ You haven’t started the form yet</h3>
                                        <p className="text-secondary">Press the play icon to start a new session.</p>
                                    </div>
                                )}
                        </div>
                    </div>
                </section>

                {this.blocks.status !== "preview" && this.settings.buttons === "sticky" && this.blocks.storyline && (
                    <nav className="navbar navbar-expand navbar-dark bg-light buttons-sticky">
                        <div className="container">
                            {buttons(this.blocks.storyline)}
                            {this.settings.pages && pages(this.blocks.storyline)}
                            {this.settings.progressbar && progressbar(this.blocks.storyline)}
                        </div>
                    </nav>
                )}
            </>
        );
    }

    /** Bind to some events. */
    componentDidMount(): void {
        this.blocks.onChange = () => {
            // Since the collector has the actual state, we need to update the component.
            // We are good React citizens. We only do this when necessary!
            this.forceUpdate();

            // If the `update` prop contains a ref of another component, update that component as well.
            // Kinda hacky approach used to keep the header (that lives as a sibling component) in sync :-)
            if (this.props.update && this.props.update.current) {
                this.props.update.current.forceUpdate();
            }
        };

        this.blocks.onFinish = (instance: Tripetto.Instance) => {
            if (this.props.onFinish) {
                this.props.onFinish(instance);
            }
        };
    }

    /** Change settings. */
    changeSettings(settings: Partial<ICollectorSettings>): void {
        this.settings.enumerators = Tripetto.isBoolean(settings.enumerators) ? settings.enumerators : this.settings.enumerators;
        this.settings.pages = Tripetto.isBoolean(settings.pages) ? settings.pages : this.settings.pages;
        this.settings.buttons = Tripetto.isString(settings.buttons) ? settings.buttons : this.settings.buttons;
        this.settings.progressbar = Tripetto.isBoolean(settings.progressbar) ? settings.progressbar : this.settings.progressbar;

        this.forceUpdate();
    }

    /** Start the collector. */
    start(): void {
        this.blocks.start();
    }

    /** Pauses the collector. */
    pause(): Tripetto.ISnapshot | undefined {
        const snapshot = this.blocks.pause();

        if (snapshot && this.props.onPause) {
            this.props.onPause(snapshot);
        }

        return snapshot;
    }

    /** Stop the collector. */
    stop(): void {
        this.blocks.stop();
    }

    /** Resets the collector. */
    reset(): void {
        this.blocks.restart(false);
    }

    /** Reloads with a new definition. */
    reload(definition: Tripetto.IDefinition): void {
        this.blocks.reload(definition);
    }
}
